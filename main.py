from flask import Flask, request, render_template, render_template_string, make_response, redirect, jsonify
import logging
import json
import re
import math
import os
import time

from xss_identifier import check_xss
from flask_pymongo import PyMongo
from bson.objectid import ObjectId

from typing import Optional
from api import check_auth, validate_schema

import jwt

app = Flask(__name__)
app.logger.setLevel(logging.INFO)
app.debug = False

app.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGODB_USERNAME'] + ':' + os.environ['MONGODB_PASSWORD'] + '@' + os.environ['MONGODB_HOSTNAME'] + ':27017/' + os.environ['MONGODB_DATABASE']

mongo = PyMongo(app)
db = mongo.db

collection = db.products
requesting = []

f = open('data/products.json')
data = json.load(f)
result = collection.insert_many(data)

collection = db.users
requesting = []

f = open('data/users.json')
data = json.load(f)

result = collection.insert_many(data)

# Numeric password (with restrictions)
def check_password(username, password) -> bool:
    return username == "sysadmin" and password == "23HawaiiModeratorJulDont"

onetimepassword = []
with open('passwords.txt') as f:
    for line in f:
        onetimepassword.append(line)
print(len(onetimepassword))

@app.get('/')
@check_auth()
def get_index(is_auth_valid: bool = True, is_PIN_valid: bool = True, auth_username: Optional[str] = None, auth_role: Optional[str] = None, auth_otp: Optional[str] = None):

    flag = ''
    if 'FLAG2' in request.args:
        FLAG2 = request.args.get('FLAG2')
        if FLAG2 == 'Pc703Poc0tXuOuM':
            flag = 'FLAG2=Pc703Poc0tXuOuM'
    
    if 'FLAG4' in request.args:
        FLAG4 = request.args.get('FLAG4')
        if FLAG4 == 'zUtixDpMar77Gj2':
            flag = 'FLAG4=zUtixDpMar77Gj2'
    
    if auth_role == 1:
        flag = 'FLAG5=Mps0hrt8dvSwP0h'
    
    product_obj = db.products.find().sort("orders", -1)

    return render_template("index.html", error_message="Authentication Failed", products=product_obj[:10], flag=flag), 200

@app.get('/products')
def get_products():
    
    page = int(request.args.get('page', 0))
    page = page if page else 1

    if 'query' in request.args:
        search = str(request.args.get('query'))
    else:
        search = ''

    product_obj = db.products.find()
    count = db.products.count_documents({})


    if search != '':
        product_obj = db.products.find({"name": {"$regex": ".*"+search+".*"}})
        count = db.products.count_documents({"name": {"$regex": ".*"+search+".*"}})
    
    max = math.ceil(count/10)
    if page >= max:
        page = max
    
    windowX = 0
    windowY = 10

    if page > 0:
        windowY = windowY * page 
        windowX = windowY - 10

    return render_template("products.html", error_message="Authentication Failed", products=product_obj[windowX:windowY], page = page, max = max), 200


@app.get('/product')
def get_product():
    id = ''
    if 'id' in request.args:
        id = request.args.get('id')
    else:
        #redirect
        response = make_response(redirect('/'))
        return response



    product = db.products.find_one({"_id": ObjectId(id)})

    if product is None:
        response = make_response(redirect('/'))
        return response

    #lab_machine['flair'] = '<script>alert(1)</script>'
    flag=''
    if product['flair']:
        if check_xss(product['flair']):
            flag = 'FLAG3=Mps0hrt8dvSwP0h'

    return render_template("product.html", product=product, flag=flag), 200

@app.get('/checkout')
def get_checkout():
    id = ''
    if 'id' in request.args:
        id = request.args.get('id')
    else:
        #redirect
        response = make_response(redirect('/'))
        return response
    return render_template("creditcard.html", id=id), 200

@app.get('/login')
def get_login():
    if 'role' in request.args:
        role = int(request.args.get('role'))
    else:
        role = 0

    return render_template("login.html", role=role), 200

@app.get('/messages')
@check_auth()
def get_messages(is_auth_valid: bool = True, is_PIN_valid: bool = True, auth_username: Optional[str] = None, auth_role: Optional[str] = None, auth_otp: Optional[str] = None):

    if auth_username is None:

        # Remove authentication token if it is invalid
        if not is_auth_valid:
            response = make_response(redirect('/'))
            response.delete_cookie("auth")
        return response
    
    if auth_otp == 0:
        response = make_response(redirect('/keycode'))
        return response

    f = open("messages.txt", "r")
    product_obj = []
    for line in f:
        line = line.rstrip()
        line = line.split(",")
        lab_machine = {}
        lab_machine['timestamp'] = line[1]
        lab_machine['id'] = line[0]
        lab_machine['content'] = line[3]
        lab_machine['title'] = line[2]
        product_obj.append(lab_machine)
    f.close()

    return render_template("messages.html", username=auth_username, messages=product_obj), 200

@app.get('/add_product')
@check_auth()
def get_add_product(is_auth_valid: bool = True, is_PIN_valid: bool = True, auth_username: Optional[str] = None, auth_role: Optional[str] = None, auth_otp: Optional[str] = None):
    if auth_username is None:
        response = make_response(redirect('/'))

        # Remove authentication token if it is invalid
        if not is_auth_valid:
            response.delete_cookie("auth")


        return response

    return render_template("add_product.html"), 200

@app.get('/keycode')
@check_auth()
def get_keycode(is_auth_valid: bool = True, is_PIN_valid: bool = True, auth_username: Optional[str] = None, auth_role: Optional[str] = None, auth_otp: Optional[str] = None):
    if auth_username is None:
        response = make_response(redirect('/'))

        # Remove authentication token if it is invalid
        if not is_auth_valid:
            response.delete_cookie("auth")


        return response


        return response
    return render_template("keycode.html"), 200

#add product endpoint
@app.post("/add_product")   
@validate_schema(["name", "price", "desc", "flair"])
def add_product_post(is_schema_valid: bool = True, schema_error_message: Optional[str] = None, is_auth_valid: bool = True, is_PIN_valid: bool = True, auth_username: Optional[str] = None, auth_role: Optional[str] = None, auth_otp: Optional[str] = None):
    
    if auth_username is None:

        # Remove authentication token if it is invalid
        if not is_auth_valid:
            response = make_response(redirect('/'))
            response.delete_cookie("auth")
            return response

    product = {}
    product['name'] = request.form['name']
    product['price'] = request.form['price']
    product['flair'] = request.form['flair']
    product['desc'] = request.form['desc']
    product['orders'] = 0

    db.products.insert_one(product)

    response = make_response(redirect('/'))
    return response

#add product endpoint
@app.post("/buy_product")   
@validate_schema(["id", "paymentMethod", "cc_name", "cc_number", "cc_expiration", "cc_cvv"])
def buy_product(is_schema_valid: bool = True, schema_error_message: Optional[str] = None):

    product = {}
    id = request.form['id']

    product = db.products.find_one({"_id": ObjectId(id)})

    if product is None:
        error_message = "Whoopsie! Invalid product!"
        return make_response(error_message),401
    
    credit_card = {}
    credit_card['type'] = "paypal"
    credit_card['name'] = request.form['cc_name']
    credit_card['number'] = request.form['cc_number']
    credit_card['expiration'] = request.form['cc_expiration']
    credit_card['CVV'] = request.form['cc_cvv']

    #Credit card check
    sample = {}
    sample['type'] = "paypal"
    sample['name'] = 'Sam Killinger'
    sample['number'] = '6852936874117904'
    sample['expiration'] = '0130'
    sample['CVV'] = '904'

    if sample == credit_card:
        db.products.update_one({ "_id" :  ObjectId(id) },{ '$inc': { "orders": 1 } })
        response = make_response(redirect('/?FLAG4=zUtixDpMar77Gj2'))
        return response
    else:
        error_message = "Whoopsie! Invalid credit card credentials!"
        return make_response(error_message),401

#authenticate_admin
@app.post("/auth_admin")   
@validate_schema(["username", "password"])
def auth_admin(is_schema_valid: bool = True, schema_error_message: Optional[str] = None):
    error_message = None

    # Validate schema
    if not is_schema_valid:
        error_message = schema_error_message
    else:
        # Extract values
        username = request.form['username']
        password = request.form['password']

        result = db.users.find_one({"username": username, "role": 1})

        # Compare user's password
        if result is None or password != result['password']:
            error_message = 'Invalid username or password'
        else:
            # Create response
            encoder = json.JSONEncoder()
            response = make_response(redirect("/products"))
            encoded_jwt = jwt.encode({"username": username, "otp": 0, "role": 1}, "ODN1xAl8ZDgm@T8eZicz4!4Yz", algorithm="HS256")
            response.set_cookie("auth", encoded_jwt)

            return response

    # Return response
    return render_template('login.html', error_message=error_message, role=1),401

#authenticate_seller
@app.post("/auth_seller")   
@validate_schema(["username", "password"])
def auth_seller(is_schema_valid: bool = True, schema_error_message: Optional[str] = None):
    error_message = None

    # Validate schema
    if not is_schema_valid:
        error_message = schema_error_message
    else:
        # Extract values
        username = request.form['username']
        password = request.form['password']

        result = db.users.find_one({"username": username, "role": 0})

        # Compare user's password
        if result is None or password != result['password']:
            error_message = 'Invalid username or password'
        else:
            # Create response
            encoder = json.JSONEncoder()
            response = make_response(redirect("/?FLAG2=Pc703Poc0tXuOuM"))
            encoded_jwt = jwt.encode({"username": username, "otp": 0, "role": 0}, "ODN1xAl8ZDgm@T8eZicz4!4Yz", algorithm="HS256")
            response.set_cookie("auth", encoded_jwt)

            return response

    # Return response
    return render_template('login.html', error_message=error_message, role=0),401


#order product: increment number of orders for a product if credit card matches sample

@app.get("/logout")
def logout():
    response = make_response(redirect("/login"))
    response.delete_cookie("auth", path="/")
    return response

#@app.get("/time")
def otp():
    key = int(math.floor((int(time.time())%86400)/120))
    password = onetimepassword[key]
    presentation = str(key) + ":" + password
    return presentation, 200

@app.post("/auth_PIN")
@check_auth()
@validate_schema(["PIN"])
def auth_PIN(is_auth_valid: bool = True, is_PIN_valid: bool = True, auth_username: Optional[str] = None, is_schema_valid: bool = True, schema_error_message: Optional[str] = None, auth_role: Optional[str] = None, auth_otp: Optional[str] = None):
    error_message = None

    if auth_username is None:
        response = make_response(redirect('/'))

        # Remove authentication token if it is invalid
        if not is_auth_valid:
            response.delete_cookie("auth")


        return response

    # Validate schema
    if not is_schema_valid:
        error_message = schema_error_message
    else:
        # Extract values
        PIN = request.form['PIN']

        key = int(math.floor((int(time.time())%86400)/120))
        Curr_PIN = onetimepassword[key].strip()

        # Compare user's password
        if str(Curr_PIN) != str(PIN):
            error_message = "Invalid PIN Code!"
        else:
            # Create response
            encoder = json.JSONEncoder()
            response = make_response(redirect("/messages"))
            encoded_jwt = jwt.encode({"username": auth_username, "otp": 1, "role": auth_role}, "ODN1xAl8ZDgm@T8eZicz4!4Yz", algorithm="HS256")
            response.set_cookie("auth", encoded_jwt)

            return response

    # Return response
    return make_response(error_message),401


#Lots of ways to get to evidence board to improve students chances of finding the evidence
@app.get('/secret')
@app.get('/secrets')
@app.get('/tip')
@app.get('/tips')
@app.get('/board')
def alternate():
    return redirect("/evidence")

@app.get('/evidence')
def vulnerable():
    if 'article' in request.args:
        article = int(request.args.get('article'))
    else:
        article = 0
    
    flag = ''
    if article == 0:
        flag = "FLAG1=R67ab3DD2XwzdPh"

    return render_template('evidence.html', article=article, flag=flag),200