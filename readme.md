# CTFx03: Wamazon Challenge

# Table of Contents
1. [Overview](#overview)
2. [Starting and Stopping the Project](#starting-and-stopping-the-project)
3. [Vulnerabilities](#vulnerabilities)
4. [Technology Used](#technology-used)

## Overview
CTFx03, dubbed the Wamazon Challenge, is an intricate Capture The Flag (CTF) created for the CTF society at Dalhousie University. The scenario involves Wamazon, a fictional rival of Amazon, in the aftermath of firing its security team under ambiguous market conditions. An insider, claiming to have proof of the company's lax security measures, has reached out with valuable hints but left the task of uncovering vulnerabilities to the participants.

## Starting and Stopping the Project

### Starting the Project
To start the project, use the following Docker Compose command. This will build and launch all the necessary containers in detached mode, allowing them to run in the background.

```docker compose up -d --build```

This command performs two main actions:

1. --build: Builds the images for the containers if they don't already exist or need updating.
2. -d: Runs the containers in detached mode, freeing up the terminal and running the containers in the background.

### Stopping the Project
When you're finished and want to stop all the running containers associated with the project, use the following command:

```docker compose down```

This command safely stops and removes all the containers, networks, and volumes created by docker compose up, cleaning up the system.

## Vulnerabilities

### Vulnerability #1: Exposed Endpoint Discovery
- **Description**: Participants need to find an exposed production endpoint containing crucial notes from the insider, requiring detailed reconnaissance of the website.

### Vulnerability #2: Directed Dictionary Attack
- **Description**: Utilizing the insider's tips, a directed dictionary attack on the website's login is required. This attack grants access to the seller's portal, allowing product listings on the platform.

### Vulnerability #3: XSS in Product Addition
- **Description**: An XSS vulnerability is found within the add product functionality on the seller portal, which could be a significant avenue for further exploitation.

### Vulnerability #4: Cracking the Message Page PIN
- **Description**: By following the insider's advice, participants can crack a complex, time-gated PIN on the messages page, uncovering more information for an advanced attack on Wamazon.

### Vulnerability #5: Admin Token Theft via XSS
- **Description**: This advanced vulnerability involves leveraging the XSS flaw, a sample credit card from the secure messages page, and custom scripting. Participants can create a script that automatically purchases a product embedded with an XSS script. This script captures the admin's token and sends it to a chosen endpoint for recording. Successfully executing this steals the CTO's admin token, granting the highest level of privilege on Wamazon and completing the CTF.

## Technology Used

- **Python**: For backend development and scripting.
- **HTML, CSS, and JavaScript**: For crafting the front-end user interface.
- **Docker**: To containerize the application, ensuring a consistent and secure testing environment.

---

**Note**: The CTFx03 Wamazon Challenge is an educational tool intended for cybersecurity training. It's crucial to engage in this exercise within ethical boundaries and in a controlled setting.
