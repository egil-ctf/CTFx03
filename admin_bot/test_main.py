import re
import os
from playwright.sync_api import Page, expect, sync_playwright
import time

#=====================CHANGE THIS========================
USERNAME = os.environ['ADMIN_USERNAME']
PASSWORD = os.environ['ADMIN_PASSWORD']
IP = os.environ['WEB_IP']
#========================================================

def test_login(page: Page):
    page.goto("http://" + IP + "/login?role=1")
    page.locator("[name=username]").fill(USERNAME)
    page.locator("[name=password]").fill(PASSWORD)
    page.get_by_role("button", name=re.compile("Login", re.IGNORECASE)).click()

    expect(page).to_have_url("http://" + IP + "/products")

    page.get_by_text("Home").click()

    expect(page).to_have_url("http://" + IP + "/")

    cookie = page.context.cookies()

    while (True):
        time.sleep(60)
        curr = page.context.cookies()   
        if curr != cookie:
            page.goto("http://" + IP + "/login?role=1")
            page.locator("[name=username]").fill(USERNAME)
            page.locator("[name=password]").fill(PASSWORD)
            page.get_by_role("button", name=re.compile("Login", re.IGNORECASE)).click()

            expect(page).to_have_url("http://" + IP + "/products")

            page.get_by_text("Home").click()

            expect(page).to_have_url("http://" + IP + "/")
        else:
            page.reload()
