FROM python:3.9

ENV FLASK_APP=main.py
ENV TZ=America/Halifax
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y sudo
RUN apt-get install -y shc
RUN apt-get install -y openssh-server
RUN useradd python

# Install dependencies
RUN pip install flask
RUN pip install Flask-PyMongo
RUN pip install beautifulsoup4
RUN pip install pyjsparser
RUN pip install PyJWT


# Change workspace
WORKDIR /app

#COPY static/ ./static/
#COPY templates/ ./templates/
#COPY main.py/ ./
#COPY api/ ./api/

# Start application
#ENTRYPOINT [ "flask", "--debug", "run", "-h", "0.0.0.0", "-p", "3000"]
ENTRYPOINT [ "flask", "run", "-h", "0.0.0.0", "-p", "3000"]

