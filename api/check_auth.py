from functools import wraps
from json import JSONDecoder
import jwt
from flask import request


def check_auth():
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):

            auth_username = None
            is_auth_valid = True
            is_PIN_valid = True
            auth_otp = None
            auth_role = None

            # Retrieve auth cookie
            if "auth" in request.cookies:
                cookie = request.cookies["auth"]

                # Attempt to parse cookie
                try:
                    body = jwt.decode(cookie, "ODN1xAl8ZDgm@T8eZicz4!4Yz", algorithms=["HS256"])
                except:
                    is_auth_valid = False
                    is_PIN_valid = False
                    body = None

                # Extract username from cookie
                if body is not None:
                    if "username" not in body:
                        is_auth_valid = False
                    else:
                        auth_username = body['username']
                        auth_otp = body['otp']
                        auth_role = body['role']
                    if "otp" not in body:
                        is_PIN_valid = False

            return f(is_auth_valid=is_auth_valid, is_PIN_valid=is_PIN_valid, auth_username=auth_username, auth_otp=auth_otp, auth_role=auth_role, *args, **kwargs)
        return wrapper
    return decorator
