from functools import wraps
from typing import List, Optional

from flask import request


def validate_schema(keys: List[str]):
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):

            # Retrieve body
            body = request.form
            body_keys = body.keys()

            # Validate body schema
            is_valid = True

            if len(body_keys) != len(keys):
                is_valid = False
            else:
                for key in body_keys:
                    if key not in keys:
                        is_valid = False
                        break

            # Return proper error message
            error_message = None

            if not is_valid:
                if len(keys) == 1:
                    error_message = f'Body must contain only {keys[0]} field'
                else:
                    keys_string = ", ".join(keys[:-1])
                    keys_string += f" and {keys[-1]}"

                    error_message = f'Body must contain only {keys_string} fields'

            return f(*args, **kwargs, is_schema_valid=is_valid, schema_error_message=error_message)
        return wrapper
    return decorator
