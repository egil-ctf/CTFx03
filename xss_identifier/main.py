from typing import List
from bs4 import BeautifulSoup
from pyjsparser import parse

valid_onload_tags = ['img', 'svg', 'body']
valid_onerror_tags = ['img', 'svg']


def _find_scripts(soup: BeautifulSoup) -> List[BeautifulSoup]:
    result = []

    all_scripts = soup.find_all("script")
    for script in all_scripts:
        if script.string is not None:
            result.append(script.string)

    return result


def _find_onerror(soup: BeautifulSoup) -> List[str]:

    def has_onerror(tag):
        return tag.has_attr('onerror') and tag.name in valid_onerror_tags

    result = []

    for tag in soup.find_all(has_onerror):
        result.append(tag['onerror'])

    return result


def _find_onload(soup: BeautifulSoup) -> List[str]:

    def has_onload(tag):
        return tag.has_attr('onload') and tag.name in valid_onload_tags

    result = []

    for tag in soup.find_all(has_onload):
        result.append(tag['onload'])

    return result


def _find_alert(tree) -> bool:
    result = False

    if tree['type'] == "ExpressionStatement":
        expression = tree['expression']

        if expression['type'] == 'CallExpression':
            arguments = expression['arguments']
            call = expression['callee']

            if call['type'] == 'Identifier' and call['name'] == 'alert':
                if arguments is not None and len(arguments) > 0:
                    return True
    else:
        if 'body' in tree:
            for element in tree['body']:
                result = result or _find_alert(element)

        if 'consequent' in tree:
            result = result or _find_alert(tree['consequent'])

        if 'alternate' in tree:
            result = result or _find_alert(tree['alternate'])

    return result


def check_xss(snippet: str) -> bool:

    # Parse document with Beautiful Soup
    doc = BeautifulSoup(snippet, "html.parser")

    # Find all possible places where XSS may be
    all = []
    all.extend(_find_scripts(doc))
    all.extend(_find_onerror(doc))
    all.extend(_find_onload(doc))

    # Try to parse all scripts
    for script in all:
        try:
            tree = parse(script)
            found = _find_alert(tree)

            if found:
                return True
        except Exception as e:
            print(f"[!] Failed to parse script: '{script}'. Error: '{e}'")

    return False
