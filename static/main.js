const inputs = document.querySelectorAll("input");
const form = document.querySelector("form");

inputs.forEach((input, key) => {
  if (key !== 0) {
    input.addEventListener("click", function () {
      inputs[0].focus();
    });
  }
  input.addEventListener("keyup", function () {
    if (input.value) {
      if (key === 7) {
        // Last one tadaa
        const userCode = [...inputs].map((input) => input.value).join("");
        
        reset()
        inputs[0].focus();

        //Send POST request to OTP checker
        //sendData({ 'PIN': userCode });
        send_fetch(userCode)
      } else {
        inputs[key + 1].focus();
      }
    }
  });
});

const reset = () => {
  form.reset();
  inputs.forEach((input, key) => {
    input.value = ""
  })
};

function sendData(data) {
    const XHR = new XMLHttpRequest();
    const FD = new FormData();
  
    // Push our data into our FormData object
    for (const [name, value] of Object.entries(data)) {
      FD.append(name, value);
    }
  
    // Define what happens on successful data submission
    XHR.addEventListener('load', (event) => {
      console.log('Yeah! Data sent and response loaded.');
    });
  
    // Define what happens in case of an error
    XHR.addEventListener('error', (event) => {
      alert('Oops! Something went wrong.');
    });
  
    // Set up our request
    XHR.open('POST', '/auth_PIN');
  
    // Send our FormData object; HTTP headers are set automatically
    XHR.send(FD);
  }

  async function send_fetch(content) {

    if (!content || content.trim() === "") {
        return;
    }

    const url = "/auth_PIN";
    const method = "POST";

    const body = new URLSearchParams();
    body.append('PIN', content);

    const response = await fetch(url, { method, body , redirect: 'follow'});

    if (response.ok) {
        console.log(response);
        if (response.redirected) {
            window.location.href = response.url;
        }
        return;
    }

    console.log("Failed to send message!");
}
